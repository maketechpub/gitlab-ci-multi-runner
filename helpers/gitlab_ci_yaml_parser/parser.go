package gitlab_ci_yaml_parser

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/common"

	"github.com/Sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type GitLabCiYamlParser struct {
	jobName   string
	config    DataBag
	jobConfig DataBag
}

type Job struct {
	DataBag
}

func (j Job) GetStage() string {
	var stage string
	var ok bool
	if stage, ok = j.GetString("stage"); !ok {
		stage = "test"
	}
	return stage
}

func (j Job) GetArtifacts() string {
	var stage string
	var ok bool
	if stage, ok = j.GetString("stage"); !ok {
		stage = "test"
	}
	return stage
}

func (j Job) GetDependencies() []string {
	a, ok := j.GetSlice("dependencies")
	if !ok {
		return []string{}
	}
	deps := make([]string, len(a))
	for i := range a {
		deps[i] = a[i].(string)
	}
	return deps
}

func (c *GitLabCiYamlParser) GetStages() []string {
	a, ok := c.config.GetSlice("stages")
	if !ok {
		return []string{"build", "test", "deploy"}
	}
	stages := make([]string, len(a))
	for i := range a {
		stages[i] = a[i].(string)
	}
	return stages
}

func (c *GitLabCiYamlParser) GetJobStage(jobName string) string {
	jobConfig, _ := c.config.GetSubOptions(jobName)
	return Job{jobConfig}.GetStage()
}

func (c *GitLabCiYamlParser) GetJobDependencies(jobName string) []string {
	jobConfig, _ := c.config.GetSubOptions(jobName)
	return Job{jobConfig}.GetDependencies()
}

func (c *GitLabCiYamlParser) GetPreviousJobsWithArtifacts() ([]string, error) {
	err := c.parseFile()
	if err != nil {
		return nil, err
	}

	allStages := c.GetStages()
	logrus.Debugln("All stages: ", allStages)

	currentStage := c.GetJobStage(c.jobName)
	dependencies := c.GetJobDependencies(c.jobName)

	logrus.Debugln("Current stage is: ", currentStage)

	previousStages := make(map[string]bool)
	currentStageVisited := false
	for _, stage := range allStages {
		if stage == currentStage {
			currentStageVisited = true
		}

		previousStages[stage] = !currentStageVisited
	}
	logrus.Debugln("Previous stages are: ", previousStages)

	var jobNames []string
	for jobName := range c.config {
		stage := c.GetJobStage(jobName)
		if previousStages[stage] && contains(dependencies, jobName) {
			jobConfig, _ := c.config.GetSubOptions(jobName)
			_, ok := jobConfig.GetSubOptions("artifacts")
			if ok {
				jobNames = append(jobNames, jobName)
			}
		}
	}
	return jobNames, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func (c *GitLabCiYamlParser) parseFile() (err error) {
	if len(c.config) > 0 {
		return nil
	}
	data, err := ioutil.ReadFile(".gitlab-ci.yml")
	if err != nil {
		return err
	}

	config := make(DataBag)
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return err
	}

	err = config.Sanitize()
	if err != nil {
		return err
	}

	c.config = config

	return
}

func (c *GitLabCiYamlParser) loadJob() (err error) {
	jobConfig, ok := c.config.GetSubOptions(c.jobName)
	if !ok {
		return fmt.Errorf("no job named %q", c.jobName)
	}

	c.jobConfig = jobConfig

	return
}

func (c *GitLabCiYamlParser) prepareJobInfo(job *common.JobResponse) (err error) {
	job.JobInfo = common.JobInfo{
		Name: c.jobName,
	}

	if stage, ok := c.jobConfig.GetString("stage"); ok {
		job.JobInfo.Stage = stage
	} else {
		job.JobInfo.Stage = "test"
	}

	return
}

func (c *GitLabCiYamlParser) getCommands(commands interface{}) (common.StepScript, error) {
	if lines, ok := commands.([]interface{}); ok {
		text := ""
		for _, line := range lines {
			if lineText, ok := line.(string); ok {
				text += lineText + "\n"
			} else {
				return common.StepScript{}, errors.New("unsupported script")
			}
		}
		return common.StepScript(strings.Split(text, "\n")), nil
	} else if text, ok := commands.(string); ok {
		return common.StepScript(strings.Split(text, "\n")), nil
	} else if commands != nil {
		return common.StepScript{}, errors.New("unsupported script")
	}

	return common.StepScript{}, nil
}

func (c *GitLabCiYamlParser) prepareSteps(job *common.JobResponse) (err error) {
	if c.jobConfig["script"] == nil {
		err = fmt.Errorf("missing 'script' for job")
		return
	}

	var scriptCommands, afterScriptCommands common.StepScript

	// get before_script
	beforeScript, err := c.getCommands(c.config["before_script"])
	if err != nil {
		return
	}

	// get job before_script
	jobBeforeScript, err := c.getCommands(c.jobConfig["before_script"])
	if err != nil {
		return
	}

	if len(jobBeforeScript) < 1 {
		scriptCommands = beforeScript
	} else {
		scriptCommands = jobBeforeScript
	}

	// get script
	script, err := c.getCommands(c.jobConfig["script"])
	if err != nil {
		return
	}
	for _, scriptLine := range script {
		scriptCommands = append(scriptCommands, scriptLine)
	}

	afterScriptCommands, err = c.getCommands(c.jobConfig["after_script"])
	if err != nil {
		return
	}

	job.Steps = common.Steps{
		common.Step{
			Name:         common.StepNameScript,
			Script:       scriptCommands,
			Timeout:      3600,
			When:         common.StepWhenOnSuccess,
			AllowFailure: false,
		},
		common.Step{
			Name:         common.StepNameAfterScript,
			Script:       afterScriptCommands,
			Timeout:      3600,
			When:         common.StepWhenAlways,
			AllowFailure: false,
		},
	}

	return
}

func (c *GitLabCiYamlParser) buildDefaultVariables(job *common.JobResponse) (defaultVariables common.JobVariables, err error) {
	defaultVariables = common.JobVariables{
		{"CI", "true", true, true, false},
		{"GITLAB_CI", "true", true, true, false},
		{"CI_SERVER_NAME", "GitLab CI", true, true, false},
		{"CI_SERVER_VERSION", "", true, true, false},
		{"CI_SERVER_REVISION", "", true, true, false},
		{"CI_PROJECT_ID", strconv.Itoa(job.JobInfo.ProjectID), true, true, false},
		{"CI_JOB_ID", strconv.Itoa(job.ID), true, true, false},
		{"CI_JOB_NAME", job.JobInfo.Name, true, true, false},
		{"CI_JOB_STAGE", job.JobInfo.Stage, true, true, false},
		{"CI_JOB_TOKEN", job.Token, true, true, false},
		{"CI_REPOSITORY_URL", job.GitInfo.RepoURL, true, true, false},
		{"CI_COMMIT_REF", job.GitInfo.Sha, true, true, false},
		{"CI_COMMIT_BEFORE_SHA", job.GitInfo.BeforeSha, true, true, false},
		{"CI_COMMIT_REF_NAME", job.GitInfo.Ref, true, true, false},
	}
	return
}

func (c *GitLabCiYamlParser) buildVariables(configVariables interface{}) (buildVariables common.JobVariables, err error) {
	if variables, ok := configVariables.(map[string]interface{}); ok {
		for key, value := range variables {
			if valueText, ok := value.(string); ok {
				buildVariables = append(buildVariables, common.JobVariable{
					Key:    key,
					Value:  valueText,
					Public: true,
				})
			} else {
				err = fmt.Errorf("invalid value for variable %q", key)
			}
		}
	} else if configVariables != nil {
		err = errors.New("unsupported variables")
	}

	return
}

func (c *GitLabCiYamlParser) prepareVariables(job *common.JobResponse) (err error) {
	job.Variables = common.JobVariables{}

	defaultVariables, err := c.buildDefaultVariables(job)
	if err != nil {
		return
	}

	job.Variables = append(job.Variables, defaultVariables...)

	globalVariables, err := c.buildVariables(c.config["variables"])
	if err != nil {
		return
	}

	job.Variables = append(job.Variables, globalVariables...)

	jobVariables, err := c.buildVariables(c.jobConfig["variables"])
	if err != nil {
		return
	}

	job.Variables = append(job.Variables, jobVariables...)

	return
}

func (c *GitLabCiYamlParser) prepareImage(job *common.JobResponse) (err error) {
	job.Image = common.Image{}
	if imageName, ok := getOption("image", c.config, c.jobConfig); ok {
		job.Image.Name = imageName.(string)
	}

	return
}

func (c *GitLabCiYamlParser) prepareServices(job *common.JobResponse) (err error) {
	job.Services = common.Services{}

	if servicesMap, ok := getOptions("services", c.config, c.jobConfig); ok {
		for _, service := range servicesMap {
			job.Services = append(job.Services, common.Image{
				Name: service.(string),
			})
		}
	}

	return
}

func (c *GitLabCiYamlParser) prepareArtifacts(job *common.JobResponse) (err error) {
	var ok bool

	artifactsMap := getOptionsMap("artifacts", c.config, c.jobConfig)

	artifactsPaths, _ := artifactsMap.GetSlice("paths")
	paths := common.ArtifactPaths{}
	for _, path := range artifactsPaths {
		paths = append(paths, path.(string))
	}

	var artifactsName string
	if artifactsName, ok = artifactsMap.GetString("name"); !ok {
		artifactsName = ""
	}

	var artifactsUntracked interface{}
	if artifactsUntracked, ok = artifactsMap.Get("untracked"); !ok {
		artifactsUntracked = false
	}

	var artifactsWhen string
	if artifactsWhen, ok = artifactsMap.GetString("when"); !ok {
		artifactsWhen = string(common.ArtifactWhenOnSuccess)
	}

	var artifactsExpireIn string
	if artifactsExpireIn, ok = artifactsMap.GetString("expireIn"); !ok {
		artifactsExpireIn = ""
	}

	job.Artifacts = make(common.Artifacts, 1)
	job.Artifacts[0] = common.Artifact{
		Name:      artifactsName,
		Untracked: artifactsUntracked.(bool),
		Paths:     paths,
		When:      common.ArtifactWhen(artifactsWhen),
		ExpireIn:  artifactsExpireIn,
	}

	return
}

func (c *GitLabCiYamlParser) prepareCache(job *common.JobResponse) (err error) {
	var ok bool

	cacheMap := getOptionsMap("cache", c.config, c.jobConfig)

	cachePaths, _ := cacheMap.GetSlice("paths")
	paths := common.ArtifactPaths{}
	for _, path := range cachePaths {
		paths = append(paths, path.(string))
	}

	var cacheKey string
	if cacheKey, ok = cacheMap.GetString("key"); !ok {
		cacheKey = ""
	}

	var cacheUntracked interface{}
	if cacheUntracked, ok = cacheMap.Get("untracked"); !ok {
		cacheUntracked = false
	}

	job.Cache = make(common.Caches, 1)
	job.Cache[0] = common.Cache{
		Key:       cacheKey,
		Untracked: cacheUntracked.(bool),
		Paths:     paths,
	}

	return
}

func (c *GitLabCiYamlParser) ParseYaml(job *common.JobResponse) (err error) {
	err = c.parseFile()
	if err != nil {
		return err
	}

	err = c.loadJob()
	if err != nil {
		return err
	}

	parsers := []struct {
		method func(job *common.JobResponse) error
	}{
		{c.prepareJobInfo},
		{c.prepareSteps},
		{c.prepareVariables},
		{c.prepareImage},
		{c.prepareServices},
		{c.prepareArtifacts},
		{c.prepareCache},
	}

	for _, parser := range parsers {
		err = parser.method(job)
		if err != nil {
			return err
		}
	}

	return nil
}

func NewGitLabCiYamlParser(jobName string) *GitLabCiYamlParser {
	return &GitLabCiYamlParser{
		jobName: jobName,
	}
}
